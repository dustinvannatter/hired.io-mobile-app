var ver = "0.39 Alpha";
var uri = "http://purchaso:shopshop!@dev.purchaso.com";
var greetings = Array('Hello', 'Howdy', 'Hey there', 'Hola', 'Salut', 'Aloha', 'Bonjour', 'Heya', 'Hi', 'Guuten takh');
var default_msg = $.mobile.loadingMessage;

function init() {
	document.addEventListener("deviceready", deviceReady, true);
	$('.ver').text(ver);
	delete init;
}

function checkPreAuth() {
    var form = $("#loginForm");
    if (localStorage["username"] != undefined && localStorage["password"] != undefined) {
        $("#username", form).val(localStorage["username"]);
        $("#password", form).val(localStorage["password"]);
        handleLogin();
    }
}

function handleLogin() {
	$.mobile.loading( 'show', {
		text: 'Logging In',
		textVisible: true,
		theme: 'e',
		html: ""
	});
    var form = $("#loginForm");    
    $("#submitButton",form).attr("disabled","disabled");
    var u = $("#username", form).val();
    var p = $("#password", form).val();
    if(u != '' && p!= '') {
        $.post(uri + "/users/connect.json", {user:u,pass:p}, function(res) {
	        console.log(res);
        	if (res.error == false) {
                localStorage["userid"] = res.user.id;
                localStorage["username"] = u;
                localStorage["password"] = p;             
                localStorage["name"] = res.user.full_name;
                localStorage["email"] = res.user.email;

				var done_load = stageData();				
                if (done_load) { $.mobile.changePage("#home", {transition: "fade"}); } 
            } else {
                $.mobile.loading('hide');
                navigator.notification.alert("Your login failed, please try again.", function() {});
            }
         $("#submitButton").removeAttr("disabled");
        },"json");
    } else {
        $.mobile.loading('hide');
        navigator.notification.alert("You must enter a username and password.", function() {});
        $("#submitButton").removeAttr("disabled");
    }
    return false;
}

function deviceReady() {
	$("#loginForm").on("submit",handleLogin);
	checkPreAuth();
}

function stageData() {
	console.log('in stageData()');

	// process motd	//////////////////////////////////////////////////////////////////////////////////////////////////
	if (localStorage["name"]) {
		var greet = greetings[Math.floor(Math.random()*greetings.length)] + " " + localStorage["name"] + ", ";
	} else {
		var greet = greetings[Math.floor(Math.random()*greetings.length)] + " " + localStorage["username"] + ", ";
	}
	$.ajax({
		url: uri + "/messages/latest.json",
		dataType: 'json',
		async: false,
		success: function(data) {
			console.log(data);
			if (data.error == false) {
				if ( (localStorage["motd_id"] == data.message.id) && (localStorage["motd_closed"] == "1") ) {
					$('.motd').hide();					
				} else {
					$('.motd_body').html(greet + data.message.message);
					localStorage["motd_id"] = data.message.id;
				}
			} else {
				$('.motd').hide();
			}
		}
	});	

	// process home	/////////////////////////////////////////////////////////////////////////////////////////////////
	$.ajax({
		url: uri + "/products/dailyDeal.json",
		dataType: 'json',
		async: false,
		success: function(data) {
			console.log(data);
			if (data.error == false) {
				$('.fd_title').html(data.deal.name);
				$('.fd_desc').html(data.deal.desc);

				var fd_img = $('<img>').attr({src: data.deal.icon.src + "&w=100"});
				$('.fd_img').html(fd_img);
				
				var dt = data.deal.expires;
				var t = dt.split(/[- :]/);
				var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
				$('.fd_countdown').countdown({until: d});
				
				$('.fd_wishlist').attr('data-shortid', data.deal.shortId);
				if (data.deal.wishlist == false) {
					$('.fd_wishlist').attr('data-status', 0);
					$('.fd_wishlist').text("Add to Wishlist");
				} else {
					$('.fd_wishlist').attr('data-status', 1);
					$('.fd_wishlist').text("Remove from Wishlist");
				}
			} else {
				$('.fd').hide();
			}
		}
	});	
	
	return true;
}

$('#profile').live('pageshow', function(event, ui) {
	var gravatar = $('<img>').attr({src: 'http://www.gravatar.com/avatar/' + $.md5(localStorage["email"])});
	$('#gravatar').html(gravatar);

	$.getJSON(uri + "/users/profile.json", function(data) {
		console.log(data);
	});
});

$('#home').live('pageshow', function(event, ui) {
});

$('#logout').live('pageshow', function(event, ui) {
	localStorage.removeItem("userid");
	localStorage.removeItem("username");
	localStorage.removeItem("password");
	localStorage.removeItem("name");
	localStorage.removeItem("email");
	
	$('#greeting').html("");
	$('.motd_body').html("");
	
    var form = $("#loginForm");
    $("#username", form).val("");
    $("#password", form).val("");
	$.mobile.changePage("#login", {transition: "flip"});
});

$('#motd').live('collapse', function() {
	localStorage["motd_closed"] = "1";
});

function fd_wishlist() {
	var short_id = $('.fd_wishlist').attr('data-shortid');
	if ($('.fd_wishlist').attr('data-status') == 1) {
	
		$.ajax({
			url: uri + "/wishlists/remove/" + short_id + ".json",
			dataType: 'json',
			async: false,
			success: function(data) {
				console.log(data);
				if (data.error == false) {
					$('.fd_wishlist').attr('data-status', 0);
					$('.fd_wishlist .ui-btn-text').text("Add to Wishlist");
					flash('Removed from Wishlist!', 2500);
				}
			}
		});		
	} else {
		$.ajax({
			url: uri + "/wishlists/add/" + short_id + ".json",
			dataType: 'json',
			async: false,
			success: function(data) {
				console.log(data);
				if (data.error == false) {
					$('.fd_wishlist').attr('data-status', 1);
					$('.fd_wishlist .ui-btn-text').text("Remove from Wishlist");
					flash('Added to Wishlist!', 2500);
				}
			}
		});		
	}
}

function flash(msg,delay,theme) {
	$('#flash h5').text(msg);
	$('#flash').fadeIn();
	setTimeout( function() { $("#flash").fadeOut(); }, delay );
}